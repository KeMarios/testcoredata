//
//  AppDelegate.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 28.02.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit
import CoreStore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let dataStack: DataStack = {
            let dataStack: DataStack = DataStack(modelName: "MyStore", bundle: Bundle.main, migrationChain: [])
            let store: SQLiteStore = SQLiteStore(fileName: "corestore.sqlite",
                                                 configuration: nil,
                                                 mappingModelBundles: [Bundle.main],
                                                 localStorageOptions: .recreateStoreOnModelMismatch)
            _ = dataStack.addStorage(store){ result in
                if !result.isSuccess {
                    print("Error setting up CoreData store")
                }
            }
            return dataStack
        }()
        
        print(NSTemporaryDirectory())
        
        CoreStore.defaultStack = dataStack
        
        print("*****************************************")
        //print("coreStoreDumpString: \(CoreStore.coreStoreDumpString)")
        //print("debugDescription: \(CoreStore.debugDescription)")
        print("entityTypesByName: \(CoreStore.entityTypesByName)")
        print("modelVersion: \(CoreStore.modelVersion)")
        print("*****************************************")
        print(" ")
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

