//
//  CoreDataObserver.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 02.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import Foundation
import UIKit
import CoreStore

protocol CoreDataObserverProtocol {
    associatedtype Entity : NSManagedObject

    var didInsertObject: (Entity, IndexPath) -> () { get set }
    var didDeleteObject: (Entity, IndexPath) -> () { get set }
    var didUpdateObject: (Entity, IndexPath) -> () { get set }
    var didMoveObject: (Entity, IndexPath, IndexPath) -> () { get set }
    var willChange: () -> () { get set }
    var didChange: () -> () { get set }
    var willRefetch: () -> () { get set }
    var didRefetch: () -> () { get set }
    
    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int) -> Int
    func objectOfIndexPath(_ indexPath: IndexPath) -> Entity
}

class CoreDataObserver<Entity: NSManagedObject>: ListObjectObserver, CoreDataObserverProtocol {
    
    let monitor: ListMonitor<Entity>
    
    var didInsertObject: (Entity, IndexPath) -> () = {_, _ in}
    var didDeleteObject: (Entity, IndexPath) -> () = {_, _ in}
    var didUpdateObject: (Entity, IndexPath) -> () = {_, _ in}
    var didMoveObject: (Entity, IndexPath, IndexPath) -> () = {_, _, _ in}
    var willChange: () -> () = {}
    var didChange: () -> () = {}
    var willRefetch: () -> () = {}
    var didRefetch: () -> () = {}
    
    init(monitor: ListMonitor<Entity>) {
        self.monitor = monitor
        self.monitor.addObserver(self)
        print("*****************************************")
        print("monitor description: \(self.monitor.debugDescription)")
        print("*****************************************")
        print(" ")
    }
    
    func numberOfSections() -> Int {
        return self.monitor.numberOfSections()
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.monitor.numberOfObjectsInSection(section)
    }
    
    func objectOfIndexPath(_ indexPath: IndexPath) -> Entity {
        return self.monitor[indexPath]
    }
    
    // MARK: ListObserver
    
    func listMonitorWillChange(_ monitor: ListMonitor<Entity>) {
        self.willChange()
    }
    
    func listMonitorDidChange(_ monitor: ListMonitor<Entity>) {
        self.didChange()
    }
    
    func listMonitorWillRefetch(_ monitor: ListMonitor<Entity>) {
        self.willRefetch()
    }
    
    func listMonitorDidRefetch(_ monitor: ListMonitor<Entity>) {
        self.didRefetch()
    }
    
    // MARK: ListObjectObserver
    
    func listMonitor(_ monitor: ListMonitor<Entity>, didInsertObject object: Entity, toIndexPath indexPath: IndexPath) {
        self.didInsertObject(object, indexPath)
    }

    func listMonitor(_ monitor: ListMonitor<Entity>, didDeleteObject object: Entity, fromIndexPath indexPath: IndexPath) {
        self.didDeleteObject(object, indexPath)
    }

    func listMonitor(_ monitor: ListMonitor<Entity>, didUpdateObject object: Entity, atIndexPath indexPath: IndexPath) {
        self.didUpdateObject(object, indexPath)
    }

    func listMonitor(_ monitor: ListMonitor<Entity>, didMoveObject object: Entity, fromIndexPath: IndexPath, toIndexPath: IndexPath) {
        self.didMoveObject(object, fromIndexPath, toIndexPath)
    }
}
