//
//  CoreDataProvider.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 02.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit
import CoreStore

class CoreDataProvider: NSObject {
    
    func addObserver<Entity: NSManagedObject>(type: Entity.Type) -> CoreDataObserver<Entity> {
        
        let observer = CoreDataObserver<Entity>(monitor: self.addMonitor(type: type))
        return observer
    }
    
    private func addMonitor<Entity: NSManagedObject>(type: Entity.Type) ->ListMonitor<Entity> {
        
        var fetchClauses: FetchClause = Where(true)
        
        switch type {
            case is Person.Type:
                fetchClauses = OrderBy(.ascending("createAt"))
                break
                
            default:
                break
        }
        
        return CoreStore.monitorList( From<Entity>(), fetchClauses)
    }
    
    func addRandomPerson() {
        _ = CoreStore.beginAsynchronous { transaction in
            
            let person = transaction.create(Into<Person>())
            person.forename = randomStringWithLength(randomInt(min: 3, max: 7)) as String
            person.surname = randomStringWithLength(randomInt(min: 3, max: 7)) as String
            person.age = Int16(randomInt(min: 10, max: 70))
            person.serverId = String(randomInt(min: 0, max: 1000))
            person.createAt = NSDate()
            
            print(" ")
            self.printPerson(person, title: "Add Random Person")
            _ = transaction.commit({ (result) -> Void in
                switch result {
                case .success(_): print("Add success!")
                case .failure(let error): print(error)
                }
            })
        }
    }
    
    func deletePerson(at person:Person) {
        _ = CoreStore.beginAsynchronous { transaction in
            
            print(" ")
            self.printPerson(person, title: "Delete Person")
            transaction.delete(person)
            _ = transaction.commit({ (result) -> Void in
                switch result {
                case .success(_): print("Delete success!")
                case .failure(let error): print(error)
                }
            })
        }
    }
    
    func deleteAllPersons() {
        _ = CoreStore.beginAsynchronous({ transaction in
            
            transaction.deleteAll(From<Person>())
            _ = transaction.commit({ (result) -> Void in
                switch result {
                case .success(_): print("Delete all success!")
                case .failure(let error): print(error)
                }
            })
        })
    }
    
    // MARK: - Private function
    
    fileprivate func printPerson(_ person: Person, title: String) {
        print("----------- \(title) -----------")
        print("id: \(person.serverId)")
        print("forename: \(person.forename)")
        print("surname: \(person.surname)")
        print("age: \(person.age)")
        print("--------------------------------")
        print(" ")
    }
}
