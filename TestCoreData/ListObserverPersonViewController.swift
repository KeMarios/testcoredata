//
//  ListObserverPersonViewController.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 01.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit

class ListObserverPersonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = ListObserverPersonViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.viewModel.viewModelDelegate = self
    }
    
    // MARK: - Actions handler
    
    @IBAction func addRandomPerson(_ sender: Any) {
        self.viewModel.addRandomPerson()
    }
    
    @IBAction func addThousandPersons(_ sender: Any) {
        self.viewModel.addPersons(1000)
    }
    
    @IBAction func deleteAllPersons(_ sender: Any) {
        self.viewModel.deleteAllPersons()
    }
    
    // MARK: - Private function
    
    func configureCell(_ cell: PersonTableViewCell, atIndexPath indexPath: IndexPath) {
        let person: Person = self.viewModel.objectOfIndexPath(indexPath)
        cell.titleLabel.text = person.serverId! + ": " + person.forename! + " " + person.surname! + " - " + String(person.age)
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PersonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PersonTableViewCell", for: indexPath) as! PersonTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.deletePerson(at: indexPath)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListObserverPersonViewController: ListObserverPersonViewModelDelegate {

    func willChange() {
        self.tableView.beginUpdates()
    }
    
    func didChange() {
        self.tableView.endUpdates()
    }
    
    func didInsertObject(object: Person, toIndexPath indexPath: IndexPath) {
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func didDeleteObject(object: Person, fromIndexPath indexPath: IndexPath) {
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func didUpdateObject(didUpdateObject object: Person, atIndexPath indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PersonTableViewCell {
            self.configureCell(cell, atIndexPath: indexPath)
        }
    }
    
    func didMoveObject(object: Person, fromIndexPath: IndexPath, toIndexPath: IndexPath) {
        self.tableView.deleteRows(at: [fromIndexPath], with: .none)
        self.tableView.insertRows(at: [toIndexPath], with: .automatic)
    }
}
