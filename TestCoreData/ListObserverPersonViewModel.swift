//
//  ListObserverPersonViewModel.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 01.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit
import CoreStore

class ListObserverPersonViewModel {

    var viewModelDelegate: ListObserverPersonViewModelDelegate?
    let coreDateProvider = CoreDataProvider()
    lazy var observer: CoreDataObserver<Person> = { [unowned self] in
        let observer = self.coreDateProvider.addObserver(type: Person.self)
        
        observer.willChange = { [unowned self] in
            print("- willChange")
            self.viewModelDelegate?.willChange()
        }
        
        observer.didChange = { [unowned self] in
            print("- didChange")
            self.viewModelDelegate?.didChange()
        }
        
        observer.willRefetch = { [unowned self] in
            print("- willRefetch")
        }
        
        observer.didRefetch = { [unowned self] in
            print("- didRefetch")
        }
        
        observer.didInsertObject = { object, indexPath in
            self.printPerson(object, title: "didInsertObject: " + String(indexPath.row))
            self.viewModelDelegate?.didInsertObject(object: object, toIndexPath: indexPath)
        }
        
        observer.didDeleteObject = { object, indexPath in
            self.printPerson(object, title: "didDeleteObject: " + String(indexPath.row))
            self.viewModelDelegate?.didDeleteObject(object: object, fromIndexPath: indexPath)
        }
        
        observer.didUpdateObject = {object, indexPath in
            self.printPerson(object, title: "didUpdateObject: " + String(indexPath.row))
            self.viewModelDelegate?.didUpdateObject(didUpdateObject: object, atIndexPath: indexPath)
        }
        
        observer.didMoveObject = {object, fromIndexPath, toIndexPath in
            self.printPerson(object, title: "didMoveObject: " + String(fromIndexPath.row) + " "  + String(toIndexPath.row))
            self.viewModelDelegate?.didMoveObject(object: object, fromIndexPath: fromIndexPath, toIndexPath: toIndexPath)
        }
        
        return observer
        }()
    
    init() {
        
    }
    
    // MARK: - Public function
    
    func addRandomPerson() {
        self.coreDateProvider.addRandomPerson()
    }
    
    func addPersons(_ quantity: Int) {
        for _ in 0...quantity {
            self.addRandomPerson()
        }
    }
    
    func deletePerson(at indexPath: IndexPath) {
        let person = self.observer.objectOfIndexPath(indexPath)
        self.coreDateProvider.deletePerson(at: person)
    }
    
    func deleteAllPersons() {
        self.coreDateProvider.deleteAllPersons()
    }
    
    func numberOfSections() -> Int {
        return self.observer.numberOfSections()
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.observer.numberOfRowsInSection(section)
    }
    
    func objectOfIndexPath(_ indexPath: IndexPath) -> Person {
        return self.observer.objectOfIndexPath(indexPath)
    }
    
    // MARK: - Private function
    
    fileprivate func printPerson(_ person: Person, title: String) {
        print("----------- \(title) -----------")
        print("id: \(person.serverId)")
        print("forename: \(person.forename)")
        print("surname: \(person.surname)")
        print("age: \(person.age)")
        print("--------------------------------")
        print(" ")
    }
}
