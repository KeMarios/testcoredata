//
//  ListObserverPersonViewModelDelegate.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 01.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit

protocol ListObserverPersonViewModelDelegate {

    func willChange()
    func didChange()
    
    func didInsertObject(object: Person, toIndexPath indexPath: IndexPath)
    func didDeleteObject(object: Person, fromIndexPath indexPath: IndexPath)
    func didUpdateObject(didUpdateObject object: Person, atIndexPath indexPath: IndexPath)
    func didMoveObject(object: Person, fromIndexPath: IndexPath, toIndexPath: IndexPath)
    
}
