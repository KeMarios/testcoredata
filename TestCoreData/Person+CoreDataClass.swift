//
//  Person+CoreDataClass.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 01.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import Foundation
import CoreData

@objc(Person)
public class Person: NSManagedObject {

}
