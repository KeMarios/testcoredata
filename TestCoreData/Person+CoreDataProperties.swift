//
//  Person+CoreDataProperties.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 01.03.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person");
    }

    @NSManaged public var age: Int16
    @NSManaged public var createAt: NSDate?
    @NSManaged public var forename: String?
    @NSManaged public var serverId: String?
    @NSManaged public var surname: String?

}
