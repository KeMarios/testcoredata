//
//  PersonTableViewCell.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 28.02.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
}
