//
//  PersonsViewController.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 28.02.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit
import CoreData
import CoreStore

class PersonsViewController: UITableViewController {

    let viewModel = PersonsViewModel()
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.sourceData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PersonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PersonTableViewCell", for: indexPath) as! PersonTableViewCell
        let person: Person = self.viewModel.sourceData[indexPath.row]
        cell.titleLabel.text = person.serverId! + ": " + person.forename! + " " + person.surname! + " - " + String(person.age)
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.deletePersonWithIndex(index: indexPath.row)
            self.viewModel.refresh()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    @IBAction func addPerson(_ sender: Any) {
        self.viewModel.addRandomPerson()
        self.viewModel.refresh()
        self.tableView.reloadData()
    }
}
