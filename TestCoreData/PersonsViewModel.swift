//
//  PersonsViewModel.swift
//  TestCoreData
//
//  Created by Mariusz Graczkowski on 28.02.2017.
//  Copyright © 2017 Mariusz Graczkowski. All rights reserved.
//

import UIKit
import CoreStore

private struct Static {
    
    static let placeDataStack: DataStack = {
        
        let dataStack: DataStack = {
            let dataStack = DataStack(modelName: "MyStore")
            try! dataStack.addStorageAndWait(SQLiteStore.self)
            
            print("*****************************************")
            //print("coreStoreDumpString: \(dataStack.coreStoreDumpString)")
            //print("debugDescription: \(dataStack.debugDescription)")
            print("custom entityTypesByName: \(dataStack.entityTypesByName)")
            print("custom modelVersion: \(dataStack.modelVersion)")
            print("*****************************************")
            print(" ")
            return dataStack
        }()
        
        return dataStack
    }()
}

class PersonsViewModel {

    var sourceData: [Person] = {
        let persons = Static.placeDataStack.fetchAll(From<Person>(),
                                                     OrderBy(.ascending("forename")))
        
        guard let newPersons = persons else {
            return []
        }
        
        return newPersons
    }()
    
    func refresh() {
        let persons = Static.placeDataStack.fetchAll(From<Person>(),
                                                     OrderBy(.ascending("forename")))
        
        guard let newPersons = persons else {
            self.sourceData = []
            return
        }
        
        self.sourceData = newPersons
    }
    
    func addRandomPerson() {
        _ = Static.placeDataStack.beginSynchronous { transaction in
            
            let person = transaction.create(Into<Person>())
            person.forename = randomStringWithLength(randomInt(min: 3, max: 7)) as String
            person.surname = randomStringWithLength(randomInt(min: 3, max: 7)) as String
            person.age = Int16(randomInt(min: 10, max: 70))
            person.serverId = String(randomInt(min: 0, max: 1000))
            person.createAt = NSDate()
            
            _ = transaction.commit()
        }
    }
    
    func deletePersonWithIndex(index: Int) {
        
        let person = self.sourceData[index]
        
        _ = Static.placeDataStack.beginSynchronous({ transaction in
            transaction.delete(person)
            _ = transaction.commit()
        })
    }
}
